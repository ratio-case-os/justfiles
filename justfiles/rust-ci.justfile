set dotenv-load
set export
HERE := env_var_or_default("HERE", invocation_directory())
IMAGE := env_var_or_default("IMAGE", "registry.gitlab.com/ratio-case-os/docker/rust-ci")
TAG := env_var_or_default("TAG", "latest")
TARGETS := env_var_or_default("TARGETS", "x86_64-unknown-linux-gnu x86_64-pc-windows-msvc")
CRATE := env_var_or_default("CRATE", "")
FLAMES := env_var_or_default("FLAMES", "")
FLAMES_DIR := env_var_or_default("FLAMES_DIR", HERE + "/target/flames")
PYTHON_CRATE := env_var_or_default("PYTHON_CRATE", CRATE + "-py")
PYTHON_DIR := env_var_or_default("PYTHON_DIR", HERE / PYTHON_CRATE)
WHEELS_DIR := env_var_or_default("WHEELS_DIR", HERE + "/target/wheels")
JUSTFILE := justfile()

# Show the recipe list.
default:
  @just --list --justfile {{JUSTFILE}}

# Pull the Docker tools image from the registry.
pull-docker:
  docker pull {{IMAGE}}:{{TAG}}

# Push the Docker tools image to the registry.
push-docker:
  docker push {{IMAGE}}:{{TAG}}

# Run the Docker tools image with the project directory mounted.
run-docker args="" cmd="":
  docker run --privileged --rm -it -v {{HERE}}:/home/ratio/work {{args}} {{IMAGE}}:{{TAG}} {{cmd}}

# Run a recipe in the Docker tools image.
in-docker recipe="":
  docker run --privileged --rm -it -v {{HERE}}:/home/ratio/work {{IMAGE}}:{{TAG}} just {{recipe}}

# Build package in release mode.
build: build-lib build-python

# Build Rust library for all available targets.
build-lib:
  cargo build --release --all-targets --color always {{ if CRATE != "" { "-p " + CRATE } else { "" } }}

# Build Python bindings for all set targets.
build-python: develop-python
  #!/bin/bash
  rm -r {{WHEELS_DIR}};\
  cd {{PYTHON_DIR}};\
  for i in {{TARGETS}};\
  do poetry run maturin build --release --zig --target "$i";\
  done

# Install Python package in development mode.
develop-python:
  cd {{PYTHON_DIR}}; poetry install; poetry run maturin develop

# Enable perf for regular users. Probably need root rights to run this.
enable-perf:
  echo -1 | sudo tee /proc/sys/kernel/perf_event_paranoid

# Create flamegraph of certain scripts for performance measurement.
flamegraph:
  #!/bin/bash
  mkdir -p {{FLAMES_DIR}}
  for i in {{FLAMES}}; \
  do CARGO_PROFILE_RELEASE_DEBUG=true cargo flamegraph -o "{{FLAMES_DIR}}/$i.svg" --no-inline --unit-test -- "$i" --color always || true; \
  done
  rm -f perf.data
  rm -f perf.data.old

# Lint both the library and Python bindings.
lint fix="": (lint-lib fix) (lint-python fix) lint-deny

# Lint the Rust code.
lint-lib fix="":
  cargo clippy --all-targets --color always {{ if CRATE != "" { "-p " + CRATE } else { "" } }} {{ if fix != "" { "--fix" } else { "" } }}

# Lint the Python bindings.
lint-python fix="": develop-python
  cargo clippy --all-targets --color always {{ if PYTHON_CRATE != "" { "-p " + PYTHON_CRATE } else { "" } }} {{ if fix != "" { "--fix" } else { "" } }}
  cd {{PYTHON_DIR}}; poetry run black . {{ if fix != "" { "" } else { "--check" } }}

# Lint project dependencies and licenses
lint-deny:
  cargo deny check

# Check for outdated packages for the library.
outdated-lib:
  cargo outdated --color always

# Build and publish both the library and Python bindings.
publish: publish-lib publish-python

# Build and publish the library.
publish-lib: build-lib
  cargo publish --color always {{ if CRATE != "" { "-p " + CRATE } else { "" } }}

# Build and publish the Python bindings.
publish-python: build-python
  twine upload target/wheels/*

# Run a single test cycle for both the library and the Python bindings.
test: test-lib-coverage test-python

# Run library tests continuously.
test-lib:
  bacon -j test {{ if CRATE != "" { "-p " + CRATE } else { "" } }}

# Run library tests with coverage.
test-lib-coverage:
  cargo tarpaulin --tests --doc --color always --locked --out Xml --output-dir target/tarpaulin {{ if CRATE != "" { "-p " + CRATE } else { "" } }}

# Run Python bindings tests.
test-python: develop-python
  cd {{PYTHON_DIR}}; poetry run pytest --cov --cov-report=term-missing -vvv -s

# Upgrade library dependencies, update Python bindings.
upgrade: upgrade-lib update-python

# Upgrade library dependencies.
upgrade-lib:
  cargo upgrade

# Update Python bindings.
update-python:
  cd {{PYTHON_DIR}}; poetry update
