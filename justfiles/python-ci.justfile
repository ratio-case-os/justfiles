set dotenv-load
set export
HERE := env_var_or_default("HERE", invocation_directory())
IMAGE := env_var_or_default("IMAGE", "registry.gitlab.com/ratio-case-os/docker/python-ci")
TAG := env_var_or_default("TAG", "latest")
JUSTFILE := justfile()
DOCS_SOURCE := HERE + "/docs/source"
DOCS_BUILD := HERE + "/docs/build"

# Show the recipe list.
default:
  @just --list --justfile {{JUSTFILE}}

# Pull the Docker tools image from the registry.
pull-docker:
  docker pull {{IMAGE}}:{{TAG}}

# Run the Docker tools image with the project directory mounted.
run-docker args="" cmd="":
  docker run --privileged --rm -it -v {{HERE}}:/home/ratio/work {{args}} {{IMAGE}}:{{TAG}} {{cmd}}

# Run a recipe in the Docker tools image.
in-docker recipe="":
  docker run --privileged --rm -it -v {{HERE}}:/home/ratio/work {{IMAGE}}:{{TAG}} just {{recipe}}

# Install the Python package in development mode.
install args="":
  poetry install {{args}}

# Update the dependencies.
update args="":
  poetry update

# Run tests using pytest.
test args="":
  poetry run pytest {{args}}

# Build the Python wheel.
build args="":
  poetry build {{args}}

# Publish the Python package with a token.
publish token:
  poetry publish --username __token__ --password {{token}}

# Build the Python package's documentation.
docs args="":
  poetry run sphinx-build {{DOCS_SOURCE}} {{DOCS_BUILD}}

# Lint the Python package.
lint args="":
  poetry run ruff check .
  poetry run black . --check

# Fix fixable linting errors.
fix args="":
  poetry run ruff check --fix .
  poetry run black . {{args}}

# Check version increment.
version args="":
  poetry run raver {{args}}

# Clean generated files.
clean:
  rm -rf {{DOCS_BUILD}}
  rm -rf {{HERE}}/dist
