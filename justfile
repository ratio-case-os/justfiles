set dotenv-load
set export
JUSTFILES := env_var_or_default("JUSTFILES", invocation_directory() + "/justfiles")
BIN_DIR := "/usr/local/bin"

# Show the recipe list.
default:
  @just --list --justfile {{justfile()}}

# Install all justfiles from the JUSTFILES directory as commands.
install:
  #!/usr/bin/env bash
  set -euo pipefail
  for justfile in `ls {{JUSTFILES}}`; do
    command=${justfile%".justfile"}
    just install-one ${command}
  done

# Install one justfile as a command. `just install-one rust-ci`
install-one command:
  #!/usr/bin/env bash
  set -euo pipefail
  cmd={{BIN_DIR}}/{{command}}
  echo -e "#!/bin/bash" > ${cmd}
  echo -e "set -euxo pipefail" >> ${cmd}
  echo -e "just --justfile {{JUSTFILES}}/{{command}}.justfile --working-directory . \"\$@\"" >> ${cmd}
  chmod +x ${cmd}
  echo "installed ${cmd}"

# Remove all commands related to the justfiles in the JUSTFILES directory from BIN_DIR.
clean:
  #!/usr/bin/env bash
  set -euo pipefail
  for justfile in `ls {{JUSTFILES}}`; do
    command=${justfile%".justfile"}
    just clean-one ${command}
  done

# Clean a single command from BIN_DIR.
clean-one command:
  #!/usr/bin/env bash
  set -euo pipefail
  cmd={{BIN_DIR}}/{{command}}
  rm -f ${cmd}
  echo "removed command ${cmd}"
